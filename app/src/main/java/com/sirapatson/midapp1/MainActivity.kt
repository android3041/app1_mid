package com.sirapatson.midapp1

import android.content.ContentValues.TAG
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun clickHello(view: View?) {
        val name:TextView = findViewById(R.id.textView)
        val id:TextView = findViewById(R.id.textView2)
        Log.d(TAG, "name :  ${name.text} | id: ${id.text}")
        val intent = Intent(this, HelloActivity2::class.java)
        intent.putExtra("name", name.text)
        startActivity(intent)
    }

}