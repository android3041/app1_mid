package com.sirapatson.midapp1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.hello_activity)

        val display:TextView=findViewById(R.id.textViewShowName)
        val name:String=intent.getStringExtra("name").toString()
        display.text=name
    }
}